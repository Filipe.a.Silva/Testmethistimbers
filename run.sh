#!/bin/bash

LIGHTHOUSEOUTPUT=/tmp/lighthouse_score.html

if [ -n "$URL" ]; then
    echo "Testing URL=$URL"
else
    echo "No URL given, testing for URL=https://google.com"
    URL="https://google.com"
fi

whoami

sudo -u chrome lighthouse --chrome-flags="--headless --disable-gpu --no-sandbox"  --no-enable-error-reporting --output html --output-path $LIGHTHOUSEOUTPUT $URL

PERFORMANCE_SCORE=$(grep -Po < $LIGHTHOUSEOUTPUT \"id\":\"performance\",\"score\":\(.*?\)} | sed 's/.*:\(.*\)}.*/\1/g')
echo "The Lighthouse Score is $PERFORMANCE_SCORE"
